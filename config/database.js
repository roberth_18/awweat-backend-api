module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: "postgres",
        host: env("DATABASE_HOST", "192.168.1.85"),
        port: env.int("DATABASE_PORT", 5976),
        database: env("DATABASE_NAME", "awweat"),
        username: env("DATABASE_USERNAME", "rxv"),
        password: env("DATABASE_PASSWORD", "180298"),
        ssl: env.bool("DATABASE_SSL", false),
      },
      options: {},
    },
  },
});
